package com.example.springbootappefk;

import co.elastic.apm.attach.ElasticApmAttacher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAppEfkApplication {

    public static void main(String[] args) {
        ElasticApmAttacher.attach();
        SpringApplication.run(SpringBootAppEfkApplication.class, args);
    }

}
