package com.example.springbootappefk.controller;

import com.example.springbootappefk.entity.Log;
import com.example.springbootappefk.repository.LogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HelloController {
    private final LogRepository logRepository;

    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        log.info("Hello {}", name);

        Log logEntity = new Log();
        logEntity.setName(name);

        logRepository.save(logEntity);

        try {
            throw new RuntimeException("Test Exception");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("exception : {}", e.toString());
        }

        return "Hello " + name;
    }
}
