FROM openjdk:17-jdk-slim

RUN groupadd -r spring-app && useradd -g spring-app spring-app

EXPOSE 8080

COPY target/spring-boot-app-efk.jar /usr/local/lib/spring-boot-app-efk.jar

RUN chown -R spring-app:spring-app /usr/local/lib/

USER spring-app

ENV TZ="Asia/Bangkok"

ENTRYPOINT ["java","-jar","-Doracle.jdbc.timezoneAsRegion=false","/usr/local/lib/spring-boot-app-efk.jar"]